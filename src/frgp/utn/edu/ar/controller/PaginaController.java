package frgp.utn.edu.ar.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.services.*;

@Controller
public class PaginaController {

	private DataAccess dataAccess;
	
	public PaginaController() {
		this.dataAccess = new DataAccess();
	}

	@RequestMapping("index.html")
	public ModelAndView eventoRedireccionarPag1()
	{
		ModelAndView MV = new ModelAndView();
		MV.setViewName("index");
		return MV;
	}

	@RequestMapping("redirigir_login.html")
	public ModelAndView welcomePage(String btnPerfil)
	{
		System.out.println(btnPerfil);
		ModelAndView MV = new ModelAndView();
		if(btnPerfil.equals("Administrador")) {
			MV.setViewName("loginAdmin");
		}
		else if(btnPerfil.equals("Cliente")) {
			MV.setViewName("loginCliente");
		}
		return MV;
	}

	@RequestMapping("procesar_login.html")
	public ModelAndView procesarLoginAdmin(String txtCodigo, String txtContrasenia, String btnSubmit) {
		ModelAndView MV = new ModelAndView();
		boolean resp = this.dataAccess.verificarDatosLogin(txtCodigo, txtContrasenia, btnSubmit);
		if(resp && btnSubmit.equals("administrador")) {
			MV.setViewName("HomePageAdmin");
		}
		else if(!resp && btnSubmit.equals("administrador")) {
			MV.setViewName("loginAdminErr");
		}
		else if(resp && btnSubmit.equals("cliente")) {
			MV.setViewName("HomePageCliente");
		}
		else if(!resp && btnSubmit.equals("cliente")) {
			MV.setViewName("loginClienteErr");
		}
		return MV;
	}

	@RequestMapping("HomePageAdmin.html")
	public ModelAndView homePageAdmin() {
		ModelAndView MV = new ModelAndView();
		MV.setViewName("HomePageAdmin");
		return MV;
	}
	
	@RequestMapping("procesar_accion_admin.html")
	public ModelAndView procesarAccionAdmin(String btnAccion) {
		ModelAndView MV = new ModelAndView();
		if(btnAccion.equals("NuevaCuenta")) {
			//Map<String,String> tiposCuentas = this.dataAccess.getTipoCuentas();
			MV.setViewName("AltaCuenta");
		}
		else if(btnAccion.equals("NuevoCliente")) {
			MV.setViewName("AltaCliente");
		}
		else if(btnAccion.equals("NuevoAdministrador")) {
			MV.setViewName("AltaAdministrador");
		}
		return MV;
	}
	
	@RequestMapping("procesar_alta_cuenta.html")
	public ModelAndView procesarAltaCuenta(String txtTipoCuenta, String txtCliente, double txtSaldoInicial) {
		ModelAndView MV = new ModelAndView();
		boolean resp = this.dataAccess.insertCuenta(txtTipoCuenta, txtCliente, txtSaldoInicial);
		if(resp) {
			MV.setViewName("AltaCuentaOk");
		}
		else {
			MV.setViewName("AltaCuentaErr");
		}
		return MV;
	}
}

