package frgp.utn.edu.ar.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Table(name="administradores")
public class Administrador implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@Column(name="codigo")
	private String codigo;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="usuario", unique=true)
	private Usuario Usuario;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="datos_personas_fisicas")
	private DatosPersonaFisica DatosPersonaFisica;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Usuario getUsuario() {
		return Usuario;
	}

	public void setUsuario(Usuario usuario) {
		Usuario = usuario;
	}

	public DatosPersonaFisica getDatosPersonaFisica() {
		return DatosPersonaFisica;
	}

	public void setDatosPersonaFisica(DatosPersonaFisica datosPersonaFisica) {
		DatosPersonaFisica = datosPersonaFisica;
	}
	
}
