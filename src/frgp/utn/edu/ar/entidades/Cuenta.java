package frgp.utn.edu.ar.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Table(name="cuentas")
public class Cuenta implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;
	
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="tipo_cuenta")
	private TipoCuenta TipoCuenta;
	
	@Column(name="codigo")
	private String codigo;

	@Column(name="descripcion")
	private String descripcion;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="cliente")
	private Cliente Cliente;
	
	@Column(name="cbu")
	private String Cbu;
	
	@Column(name="saldo")
	private double Saldo;
	
	@Column(name="nro_cuenta")
	private String NroCuenta;
	
	@Column(name="nombre")
	private String Nombre;
	
	@Column(name="fecha_creacion")
	private int FechaCreacion;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public TipoCuenta getTipoCuenta() {
		return TipoCuenta;
	}

	public void setTipoCuenta(TipoCuenta tipoCuenta) {
		TipoCuenta = tipoCuenta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Cliente getCliente() {
		return Cliente;
	}

	public void setCliente(Cliente cliente) {
		Cliente = cliente;
	}

	public String getCbu() {
		return Cbu;
	}

	public void setCbu(String cbu) {
		Cbu = cbu;
	}

	public double getSaldo() {
		return Saldo;
	}

	public void setSaldo(double saldo) {
		Saldo = saldo;
	}

	public String getNroCuenta() {
		return NroCuenta;
	}

	public void setNroCuenta(String nroCuenta) {
		NroCuenta = nroCuenta;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public int getFechaCreacion() {
		return FechaCreacion;
	}

	public void setFechaCreacion(int fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}

	
}
