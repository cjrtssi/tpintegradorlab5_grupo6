package frgp.utn.edu.ar.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="cuotas")
public class Cuota implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="prestamo")
	private Prestamo prestamo;
	
	@Column(name="nro_cuota")
	private int NroCuota;
	
	@Column(name="fecha_pago")
	private int FechaPago;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Prestamo getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}
	
	public int getNroCuota() {
		return NroCuota;
	}
	
	public void setNroCuota(int NroCuota) {
		this.NroCuota = NroCuota;
	}

	
	public int getFechaPago() {
		return FechaPago;
	}

	public void setFechaPago(int FechaPago) {
		this.FechaPago = FechaPago;
	}

}
