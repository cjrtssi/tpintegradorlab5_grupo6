package frgp.utn.edu.ar.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Table(name="prestamos")
public class Prestamo implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="cliente")
	private Cliente Cliente;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="movimiento")
	private Movimiento Movimiento;

	@Column(name="fecha")
	private int Fecha;

	@Column(name="cuotas")
	private int Cuotas;
	
	@Column(name="plazo")
	private int Plazo;
	
	@Column(name="monto_por_mes")
	private double MontoPorMes;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Cliente getCliente() {
		return Cliente;
	}

	public void setCliente(Cliente cliente) {
		Cliente = cliente;
	}

	public Movimiento getMovimiento() {
		return Movimiento;
	}

	public void setMovimiento(Movimiento movimiento) {
		Movimiento = movimiento;
	}

	public int getFecha() {
		return Fecha;
	}

	public void setFecha(int fecha) {
		Fecha = fecha;
	}

	public int getCuotas() {
		return Cuotas;
	}

	public void setCuotas(int cuotas) {
		Cuotas = cuotas;
	}

	public int getPlazo() {
		return Plazo;
	}

	public void setPlazo(int plazo) {
		Plazo = plazo;
	}

	public double getMontoPorMes() {
		return MontoPorMes;
	}

	public void setMontoPorMes(double montoPorMes) {
		MontoPorMes = montoPorMes;
	}

}
