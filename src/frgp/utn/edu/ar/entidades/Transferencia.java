package frgp.utn.edu.ar.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Table(name="transferencias")
public class Transferencia implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="cliente")
	private Cliente Cliente;
	
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="mov_saldo_negativo")
	private Movimiento SaldoNegativo;

	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="mov_saldo_positivo")
	private Movimiento SaldoPositivo;
	
	@Column(name="fecha")
	private int Fecha;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Cliente getCliente() {
		return Cliente;
	}

	public void setCliente(Cliente cliente) {
		Cliente = cliente;
	}

	public Movimiento getSaldoNegativo() {
		return SaldoNegativo;
	}

	public void setSaldoNegativo(Movimiento saldoNegativo) {
		SaldoNegativo = saldoNegativo;
	}

	public Movimiento getSaldoPositivo() {
		return SaldoPositivo;
	}

	public void setSaldoPositivo(Movimiento saldoPositivo) {
		SaldoPositivo = saldoPositivo;
	}

	public int getFecha() {
		return Fecha;
	}

	public void setFecha(int fecha) {
		Fecha = fecha;
	}

	
}
