package frgp.utn.edu.ar.main;

import frgp.utn.edu.ar.entidades.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Main 
{
    public static void main( String[] args )
    {
        SessionFactory sessionFactory;
        
        Configuration configuration = new Configuration();
        configuration.configure();
        
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session session = sessionFactory.openSession();
        
        session.beginTransaction();        


        //Inserto tipos de movimientos
        TipoMovimiento tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("alta_cuenta");
        tipoMovimiento.setDescripcion("Alta de cuenta");
        session.save(tipoMovimiento);

        tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("alta_prestamo");
        tipoMovimiento.setDescripcion("Alta de prestamo");
        session.save(tipoMovimiento);
        
        tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("pago_prestamo");
        tipoMovimiento.setDescripcion("Pago de prestamo");
        session.save(tipoMovimiento);
        
        tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("transferencia");
        tipoMovimiento.setDescripcion("Transferencia");
        session.save(tipoMovimiento);
        
        
        
        //Inserto tipos de cuentas
        TipoCuenta tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("ca_pesos");
        tipoCuenta.setDescripcion("Caja de ahorro en pesos");
        session.save(tipoCuenta);

        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("ca_dolares");
        tipoCuenta.setDescripcion("Caja de ahorro en dolares");
        session.save(tipoCuenta);
        
        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("cc");
        tipoCuenta.setDescripcion("Cuenta corriente");
        session.save(tipoCuenta);
        
        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("cc_esp_pesos");
        tipoCuenta.setDescripcion("Cuenta corriente especial en pesos");
        session.save(tipoCuenta);

        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("cc_esp_dolares");
        tipoCuenta.setDescripcion("Cuenta corriente especial en dolares");
        session.save(tipoCuenta);

        
        //Inserto tipos de usuario
        TipoUsuario tipoAdministrador = new TipoUsuario();
        tipoAdministrador.setCodigo("administrador");
        tipoAdministrador.setDescripcion("Administrador");
        session.save(tipoCuenta);

        TipoUsuario tipoCliente = new TipoUsuario();
        tipoCliente.setCodigo("cliente");
        tipoCliente.setDescripcion("Cliente");
        session.save(tipoCuenta);

        //Inserto usuarios, clientes y administradores
        Usuario usuario = new Usuario();
        Cliente cliente = new Cliente();
        DatosPersonaFisica datosPersonaFisica = new DatosPersonaFisica();
        Administrador administrador = new Administrador();
        Cuenta cuenta = new Cuenta();
        
        datosPersonaFisica.setApellido("de la Torre");
        datosPersonaFisica.setNombre("Joaquin Lino");
        datosPersonaFisica.setDni("33333333");
        datosPersonaFisica.setDireccion("Calle X 2222 - Caba");
        datosPersonaFisica.setFechaNacimiento(19889424);
        
        usuario.setCodigoUsuario("jdelatorre");
        usuario.setContrasenia("jdelatorre");
        usuario.setTipoUsuario(tipoAdministrador);
        
        datosPersonaFisica.setApellido("de la Torre");
        datosPersonaFisica.setNombre("Joaquin Lino");
        datosPersonaFisica.setDni("33333333");
        datosPersonaFisica.setDireccion("Calle X 2222 - Caba");
        datosPersonaFisica.setFechaNacimiento(19889424);
        
        administrador.setCodigo("jdelatorre");
        administrador.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("fconde");
        usuario.setContrasenia("fconde");
        usuario.setTipoUsuario(tipoCliente);
        
        datosPersonaFisica.setApellido("Conde");
        datosPersonaFisica.setNombre("Fernanda");
        datosPersonaFisica.setDni("11111111");
        datosPersonaFisica.setDireccion("Calle 1 1111");
        datosPersonaFisica.setFechaNacimiento(19790101);        
        
        cliente.setCodigo("fconde");
        cliente.setUsuario(usuario);
        
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("eamadeo");
        usuario.setContrasenia("eamadeo");
        usuario.setTipoUsuario(tipoCliente);
        
        datosPersonaFisica.setApellido("Amadeo");
        datosPersonaFisica.setNombre("Estela");
        datosPersonaFisica.setDni("22222222");
        datosPersonaFisica.setDireccion("Calle 2 2222");
        datosPersonaFisica.setFechaNacimiento(19920202);

        cliente.setCodigo("eamadeo");
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("mgarcia");
        usuario.setContrasenia("mgarcia");
        usuario.setTipoUsuario(tipoCliente);
        
        datosPersonaFisica.setApellido("Garcia");
        datosPersonaFisica.setNombre("Maria");
        datosPersonaFisica.setDni("33333333");
        datosPersonaFisica.setDireccion("Calle 3 3333");
        datosPersonaFisica.setFechaNacimiento(19930303);
        
        cliente.setCodigo("mgarcia");
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("osiames");
        usuario.setContrasenia("osiames");
        usuario.setTipoUsuario(tipoCliente);
        
        datosPersonaFisica.setApellido("Siames");
        datosPersonaFisica.setNombre("Ozzito");
        datosPersonaFisica.setDni("44444444");
        datosPersonaFisica.setDireccion("Calle 4 4444");
        datosPersonaFisica.setFechaNacimiento(20130101);
        
        cliente.setCodigo("osiames");
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("uovejera");
        usuario.setContrasenia("uovejera");
        usuario.setTipoUsuario(tipoCliente);
        
        datosPersonaFisica.setApellido("Ovejera");
        datosPersonaFisica.setNombre("Uma");
        datosPersonaFisica.setDni("55555555");
        datosPersonaFisica.setDireccion("Calle 5 5555");
        datosPersonaFisica.setFechaNacimiento(20121505);
        
        cliente.setCodigo("uovejera");        
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("cdelatorre");
        usuario.setContrasenia("cdelatorre");
        usuario.setTipoUsuario(tipoCliente);
        
        datosPersonaFisica.setApellido("de la Torre");
        datosPersonaFisica.setNombre("Carlos Lino");
        datosPersonaFisica.setDni("66666666");
        datosPersonaFisica.setDireccion("Calle 6 6666");
        datosPersonaFisica.setFechaNacimiento(19160101);
        
        cliente.setCodigo("cdelatorre");
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);
        

        
        //Inserto cuentas y los movimientos de alta
        String hql = "from TipoCuenta where codigo='ca_pesos'";
        TipoCuenta ca_pesos = (TipoCuenta) session.createQuery(hql).uniqueResult();
        hql = "from Cliente where codigo='jdelatorre'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        hql = "from TipoMovimiento where codigo='alta_cuenta'";
        TipoMovimiento alta_cuenta = (TipoMovimiento) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("11111111");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("11111111");
        cuenta.setDescripcion("");
        cuenta.setCbu("1111111111111111111111");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 11111111");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);

        Movimiento movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(11111.33);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);


        hql = "from Cliente where codigo='uovejera'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("22222222");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("22222222");
        cuenta.setDescripcion("");
        cuenta.setCbu("2222222222222222222222");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 22222222");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);
        
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(22332.654);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);


        hql = "from Cliente where codigo='eamadeo'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("33333333");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("33333333");
        cuenta.setDescripcion("");
        cuenta.setCbu("3333333333333333333333");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 33333333");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);

        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(64573563);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);

        
        
        hql = "from Cliente where codigo='cdelatorre'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("44444444");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("44444444");
        cuenta.setDescripcion("");
        cuenta.setCbu("4444444444444444444444");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 44444444");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);
        
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(12312.123);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);

        
        hql = "from Cliente where codigo='eamadeo'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("55555555");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("55555555");
        cuenta.setDescripcion("");
        cuenta.setCbu("5555555555555555555555");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 55555555");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);
        
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(12345.123);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);

        

        //Inserto prestamos y cuotas
        Prestamo prestamo = new Prestamo();
        Cuota cuota;
        hql = "from Cliente where codigo='cdelatorre'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        hql = "from TipoMovimiento where codigo='alta_prestamo'";
        TipoMovimiento alta_prestamo = (TipoMovimiento) session.createQuery(hql).uniqueResult();

        double importe = 30000;
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_prestamo);
        movimiento.setDetalle("alta de prestamo");
        movimiento.setFecha(20200607);
        movimiento.setImporte(importe);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);
        
        prestamo.setCliente(cliente);
        prestamo.setMovimiento(movimiento);
        prestamo.setCuotas(3);
        prestamo.setFecha(20200605);
        prestamo.setMontoPorMes(importe / prestamo.getCuotas());
        prestamo.setPlazo(3);

        for(int i = 1; i <= prestamo.getCuotas(); i++) {
            cuota = new Cuota();
            cuota.setPrestamo(prestamo);
            cuota.setNroCuota(i);
            session.save(cuota);        	
        }

        prestamo = new Prestamo();
        hql = "from Cliente where codigo='uovejera'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();

        importe = 1200;
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_prestamo);
        movimiento.setDetalle("alta de prestamo");
        movimiento.setFecha(20200607);
        movimiento.setImporte(importe);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);
        
        prestamo.setCliente(cliente);
        prestamo.setMovimiento(movimiento);
        prestamo.setCuotas(4);
        prestamo.setFecha(20200605);
        prestamo.setMontoPorMes(importe / prestamo.getCuotas());
        prestamo.setPlazo(4);
        
        for(int i = 1; i <= prestamo.getCuotas(); i++) {
            cuota = new Cuota();
            cuota.setPrestamo(prestamo);
            cuota.setNroCuota(i);
            session.save(cuota);        	
        }

        prestamo = new Prestamo();
        hql = "from Cliente where codigo='eamadeo'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();

        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_prestamo);
        movimiento.setDetalle("alta de prestamo");
        movimiento.setFecha(20200607);
        movimiento.setImporte(543634);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);
        
        importe = 6000;
        prestamo.setMovimiento(movimiento);
        prestamo.setCliente(cliente);
        prestamo.setCuotas(2);
        prestamo.setFecha(20200605);
        prestamo.setMontoPorMes(importe / prestamo.getCuotas());
        prestamo.setPlazo(2);

        for(int i = 1; i <= prestamo.getCuotas(); i++) {
            cuota = new Cuota();
            cuota.setPrestamo(prestamo);
            cuota.setNroCuota(i);
            session.save(cuota);        	
        }

        
        hql = "from TipoMovimiento where codigo='transferencia'";
        TipoMovimiento tipo_transferencia = (TipoMovimiento) session.createQuery(hql).uniqueResult();
        hql = "from Cliente where codigo='osiames'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        
        hql = "from Cuenta where Cliente = '" + cliente.getId() + "'";
        Cuenta origen = (Cuenta) session.createQuery(hql).uniqueResult();
        
        hql = "from Cuenta where Cbu='5555555555555555555555'";
        Cuenta destino = (Cuenta) session.createQuery(hql).uniqueResult();
        
        Transferencia transferencia = new Transferencia();
        importe = 543634;
        transferencia.setCliente(cliente);
        
        Movimiento movimientoNeg = new Movimiento();
        movimientoNeg.setTipoMovimiento(tipo_transferencia);
        movimientoNeg.setDetalle("transferencia - saldo negativo");
        movimientoNeg.setFecha(20200607);
        movimientoNeg.setImporte(-importe);
        movimientoNeg.setCuenta(origen);
        session.save(movimientoNeg);

        Movimiento movimientoPos = new Movimiento();
        movimientoPos.setTipoMovimiento(tipo_transferencia);
        movimientoPos.setDetalle("transferencia - saldo positivo");
        movimientoPos.setFecha(20200607);
        movimientoPos.setImporte(importe);
        movimientoPos.setCuenta(destino);
        session.save(movimientoPos);

        transferencia.setSaldoNegativo(movimientoNeg);
        transferencia.setSaldoPositivo(movimientoPos);
        transferencia.setFecha(20200608);
        session.save(transferencia);
        
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}
