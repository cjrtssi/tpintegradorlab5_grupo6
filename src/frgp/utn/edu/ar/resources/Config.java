package frgp.utn.edu.ar.resources;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;

import frgp.utn.edu.ar.entidades.*;

@Configuration
public class Config {

	@Bean(initMethod="initPersona", destroyMethod="destroyPersona")
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Cliente cl1() {
		Cliente cli = new Cliente();
		return cli;
	}

}
