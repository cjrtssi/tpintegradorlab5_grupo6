package frgp.utn.edu.ar.services;

import frgp.utn.edu.ar.entidades.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;


public class DataAccess
{
	private SessionFactory sessionFactory;
	private Session session;

	public void initSession() {
	    Configuration configuration = new Configuration();
	    configuration.configure();
	    ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
	    this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	    this.session = sessionFactory.openSession();
	}
	
	public void closeSession() {
	    this.session.close();
	    this.sessionFactory.close();
	}
	
	public String getRandomIntegerString(int len, int max){
		  Random r = new Random();
		  String aRandom = "";
		  for(int i = 0; i < len; i++){
		    aRandom += String.valueOf(r.nextInt(max)+1);
		  }
		  return aRandom;
		}

	public boolean verificarDatosLogin(String codigo, String contrasenia, String tipoUsuario)
	{
		boolean resp = false;
		this.initSession();
		TipoUsuario tusu = new TipoUsuario();
		String hql = "from TipoUsuario where codigo = '" + tipoUsuario + "'";
		tusu = (TipoUsuario) this.session.createQuery(hql).uniqueResult();
		if(tusu != null) {
			hql = "from Usuario where codigo_usuario='" + codigo + "' and contrasenia='" + contrasenia + "' and tipo_usuario='" + tusu.getId() + "'";
			Usuario u = (Usuario) session.createQuery(hql).uniqueResult();
			if(u != null) {
				resp = true;
			}
		}
		else {
			System.out.println("Error de datos, ver como trabajar excepcion. No está el registro 'administrador' en tipos de usuarios");
		}
	    this.closeSession();
		return resp;
	}

	public Map<String, String> getTipoCuentas(){
		this.initSession();
		String hql = "select codigo, descripcion from TipoCuenta";
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> tiposCuentas = (List<Object[]>) this.session.createQuery(hql).list();
		String key = new String();
		String value = new String();
		for( Object[] obj : tiposCuentas) {
			key = (String)obj[0];
			value = (String)obj[1];
			map.put( key, value );
		}
		this.closeSession();
		return map;
	}
	
	public TipoCuenta getTipoCuenta(String codigo) {
		TipoCuenta tc = new TipoCuenta();
		this.initSession();
		String hql = "from TipoCuenta where codigo = '" + codigo + "'";
		tc = (TipoCuenta)this.session.createQuery(hql).uniqueResult();
		this.closeSession();
		return tc;
	}
	public Usuario getUsuario(String usuario) {
		Usuario usu = new Usuario();
		this.initSession();
		String hql = "from Usuario where codigo = '" + usuario + "'";
		usu = (Usuario)this.session.createQuery(hql).uniqueResult();
		this.closeSession();
		return usu;
	}
	public Cliente getCliente(String cliente) {
		Cliente cli = new Cliente();
		this.initSession();
		String hql = "from Cliente where codigo = '" + cliente + "'";
		cli = (Cliente)this.session.createQuery(hql).uniqueResult();
		this.closeSession();
		return cli;
	}
	public boolean insertCuenta(String tipoCuenta, String cliente, double saldoInicial) {
		boolean resp = true;
		Cuenta cta = new Cuenta();
		TipoCuenta tcta = new TipoCuenta();
		tcta = this.getTipoCuenta(tipoCuenta);
		Cliente cli = new Cliente();
		cli = this.getCliente(cliente);
		if(	tcta != null &&
			cli != null &&
			saldoInicial > 0) {
			String nroCuenta = "222" + this.getRandomIntegerString(9, 9);
			String codCliente = cli.getCodigo();
			String codtcta = tcta.getCodigo();
			String codigo = codCliente + "-" + codtcta + "-" + nroCuenta;
			String cbu = this.getRandomIntegerString(22, 9);
			cta.setNroCuenta(nroCuenta);
			cta.setNombre(codigo);
			cta.setCodigo(codigo);
			cta.setDescripcion(codigo);
			cta.setCliente(cli);
			cta.setTipoCuenta(tcta);
			cta.setSaldo(saldoInicial);
			cta.setCbu(cbu);
			cta.setFechaCreacion(20200101);
			this.initSession();
			this.session.beginTransaction();
			this.session.save(cta);
			this.session.getTransaction().commit();
			this.closeSession();
		}
		else{
			resp = false;
		}
		return resp;
	}
}
